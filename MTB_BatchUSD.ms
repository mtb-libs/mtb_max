__author__ = "Mel Massadian"
__email__ = "mel@melmassadian.com"
__doc__ = "Batch USD Exporter"
__version__ = "1.2.0"


rollout BatchUSD "MTB BATCH USD 1.2" width:320 height:160
(
	local get_files
	local set_path
	local root_path
	local pattern




	edittext edt1 "root" pos:[104,16] width:192 height:16 readonly:false
	button btn1 "Browse root" pos:[8,13] width:88 height:24
	edittext edt3 "pattern" pos:[104,41] width:192 height:16 readonly:false
	edittext edt2 "" pos:[104,71] width:192 height:16 readonly:false
	button btn2 "Save to  ..." pos:[8,71] width:88 height:24
	button btn3 "Convert" pos:[192,95] width:104 height:40
	-- hyperlink hp1 "melmass" address:"https://melmassadian.com" color:(color 0 190 255) hovercolor:white visitedColor:(color 0 190 255) align:#right

	on btn1 pressed do
	(
		-- local OpenFileDialog = dotnetobject "System.Windows.Forms.OpenFileDialog"
		-- OpenFileDialog.multiselect = true
		-- OpenFileDialog.title = "Open (.MAX) files"
		-- OpenFileDialog.filter = "3ds Max (*.max)|*.max|All files (*.*)|*.*"
		-- OpenFileDialog.ShowDialog()

		-- if OpenFileDialog.FileNames.count > 0 then
		-- (
		-- 	get_files = OpenFileDialog.FileNames
		-- 	get_path = GetFileNamePath OpenFileDialog.FileNames[1]
		-- 	edt1.text = (get_path + " (" + (get_files.count as string) + " .MAX files)")
		-- )
		root_path = getSavePath caption:"Root folder"
		if root_path != undefined then
		(
		edt1.text = root_path
		)
	)

	on btn2 pressed do
	(
		set_path = getSavePath caption:"Save to ..."
		if set_path != undefined then
		(
			edt2.text = set_path
		)
	)
	on edt1 changed txt do
	(
		root_path = txt
	)

	on edt2 changed txt do
	(
		set_path = txt
	)
	on edt3 changed txt do
	(
		pattern = txt
	)

	on btn3 pressed do
	(

		if root_path != undefined and set_path != undefined then
		(

			-- Common Omniverse settings
			nvOmniverse.Connect()
			nvOmniverse.SetOptionVar "mdl_copy_option" "2"
			nvOmniverse.SetOptionVar "include_usd_preview_surface" "true"
			nvOmniverse.SetOptionVar "enable_renaming" "true"

			get_files = getFilesRecursive root_path pattern

			total = get_files.count
			progressStart "Exporting Max Files to USD..."
			n = 0
			for i in get_files do
			(
				if not (progressUpdate(100.*n/total)) then exit

				if (i == undefined) then continue

				setVRaySilentMode() true

				loadMaxFile i quiet:true

				-- For new Vray >= 3.3 behavior:
				-- renderers.current.twoLevel_adaptiveMethod=1
				-- progressive sampler gradually reduces the noise threshold when 30% of all pixels are left
				-- renderers.current.progressive_dynNoiseThreshold=30
				-- 1=new sRGB
				-- renderers.current.options_rgbColorSpace=1
				-- renderers.current.dmc_useLocalSubdivs=false
				--renderers.current.gi_reflectCaustics=true
				-- renderers.current.imageSampler_shadingRate=6

				-- USD Options
				nvOmniverse.SetOptionVar "mdl_copy_option" "2"
				nvOmniverse.SetOptionVar "include_usd_preview_surface" "true"
				nvOmniverse.SetOptionVar "enable_renaming" "true"
				nvOmniverse.SetOptionVar "selection_mode" "1"

				nvOmniverse.CreateURL (set_path + "/" + (GetFileNameFile i) + ".usd")


				n = n+1
			)
			progressEnd()
			resetMaxFile #noprompt

		)
		else
		(
			messagebox "Not specified path!" title:"Path"
		)

	)
	on BatchUSD open do edt3.text = "*.max"

)
createDialog BatchUSD
