import MaxPlus


def solidMaterial(color):
    m = MaxPlus.Factory.CreateDefaultStdMat()
    m.Ambient = color
    m.Diffuse = color
    m.Specular = MaxPlus.Color(1, 1, 1)
    m.Shininess = 0.5    
    m.ShinyStrength = 0.7 
    return m

def descendants(node):
    for c in node.Children:
        yield c
        for d in descendants(c):
            yield d


def allNodes():
    return descendants(MaxPlus.Core.GetRootNode())    

def applyMaterialToNodes(m, nodes = allNodes()):
    for n in nodes:
        n.Material = m    


def listAssetsInFile():
  assetTypes = { 
    MaxPlus.AssetType.OtherAsset : "other",
    MaxPlus.AssetType.BitmapAsset : "bitmap",         
    MaxPlus.AssetType.XRefAsset : "xref",                    
    MaxPlus.AssetType.PhotometricAsset : "photometric",
    MaxPlus.AssetType.AnimationAsset : "animation",        
    MaxPlus.AssetType.VideoPost : "video post",                     
    MaxPlus.AssetType.BatchRender : "batch render",           
    MaxPlus.AssetType.ExternalLink : "external link",          
    MaxPlus.AssetType.RenderOutput : "render output",          
    MaxPlus.AssetType.PreRenderScript : "pre-render script",       
    MaxPlus.AssetType.PostRenderScript : "post-render script",      
    MaxPlus.AssetType.SoundAsset : "sound",            
    MaxPlus.AssetType.ContainerAsset : "container",        
  }  

  assets = MaxPlus.AssetManager.GetAssets()
  print ("There are ", len(assets), " assets")
  for i in range(len(assets)):
      a = assets[i]
      print ("Asset", i, a.ResolvedFileName, " is a ", assetTypes[a.Type])
      print ("     ", a.SpecifiedFileName)     