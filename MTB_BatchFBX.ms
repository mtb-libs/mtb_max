__author__ = "Mel Massadian"
__email__ = "mel@melmassadian.com"
__doc__ = "Batch FBX Exporter"
__version__ = "1.2.0"


rollout BatchFBX "MTB BATCH FBX 1.2" width:320 height:160
(
	local get_path
	local get_files
	local set_path

	edittext edt1 "" pos:[104,16] width:192 height:16 readonly:true
	button btn1 "Open from ..." pos:[8,13] width:88 height:24
	edittext edt2 "" pos:[104,51] width:192 height:16 readonly:true
	button btn2 "Save to  ..." pos:[8,48] width:88 height:24
	button btn3 "Convert" pos:[192,80] width:104 height:40
	-- hyperlink hp1 "melmass" address:"https://melmassadian.com" color:(color 0 190 255) hovercolor:white visitedColor:(color 0 190 255) align:#right

	on btn1 pressed do
	(
		local OpenFileDialog = dotnetobject "System.Windows.Forms.OpenFileDialog"
		OpenFileDialog.multiselect = true
		OpenFileDialog.title = "Open (.MAX) files"
		OpenFileDialog.filter = "3ds Max (*.max)|*.max|All files (*.*)|*.*"
		OpenFileDialog.ShowDialog()

		if OpenFileDialog.FileNames.count > 0 then
		(
			get_files = OpenFileDialog.FileNames
			get_path = GetFileNamePath OpenFileDialog.FileNames[1]
			edt1.text = (get_path + " (" + (get_files.count as string) + " .MAX files)")
		)
	)

	on btn2 pressed do
	(
		set_path = getSavePath caption:"Save to ..."
		if set_path != undefined then
		(
			edt2.text = set_path
		)
	)

	on btn3 pressed do
	(

		if get_files != undefined and set_path != undefined then
		(
			total = get_files.count
			progressStart "Exporting Max Files to FBX..."
			n = 0
			for i in get_files do
			(
				if not (progressUpdate(100.*n/total)) then exit
				-- VRAY Fix
				setVRaySilentMode() true

				loadMaxFile i quiet:true

				-- For new Vray >= 3.3 behavior:
				-- renderers.current.twoLevel_adaptiveMethod=1
				-- progressive sampler gradually reduces the noise threshold when 30% of all pixels are left
				-- renderers.current.progressive_dynNoiseThreshold=30
				-- 1=new sRGB
				-- renderers.current.options_rgbColorSpace=1
				-- renderers.current.dmc_useLocalSubdivs=false
				--renderers.current.gi_reflectCaustics=true
				-- renderers.current.imageSampler_shadingRate=6

				-- FBX Options
				FBXExporterSetParam "ASCII" false
				FBXExporterSetParam "FileVersion" "FBX201400"
				FBXExporterSetParam "Animation" true
				FBXExporterSetParam "Removesinglekeys" true
				FBXExporterSetParam "EmbedTextures" true

				FBXExporterSetParam "Triangulate" false
				FBXExporterSetParam "Shape" true
				FBXExporterSetParam "Skin" true
				FBXExporterSetParam "TangentSpaceExport" true
				FBXExporterSetParam "AxisConversionMethod" #animation
				FBXExporterSetParam "SmoothingGroups" true
				FBXExporterSetParam "UseSceneName" true
				FBXExporterSetParam "ShowWarnings" false
				FBXExporterSetParam "UpAxis" "Y"

				exportFile (set_path + "\\" + (GetFileNameFile i) + ".fbx") #noPrompt
				n = n+1
			)
			progressEnd()
			resetMaxFile #noprompt

		)
		else
		(
			messagebox "Not specified path!" title:"Path"
		)
	)

)
createDialog BatchFBX
