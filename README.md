# MTB Max

3DS Max related tools.

## MTB Batch FBX
Opinionated batch FBX exporter. You select all your input max files and it will batch export them using our in house settings (customize it to yours)